<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Página principal
Route::get('/', 'PageController@home')->name('visitor.dashboard.index');

// Autenticación Visitor
Route::auth();


// Grupo Visitor
Route::group([
    'prefix' => 'user',
    'namespace' => 'User',
    'middleware' => 'auth'],
    function(){
        Route::get('/', 'DashboardController@index')->name('user.dashboard');
        Route::get('/my_account', 'DashboardController@my_account')->name('user.my_account');
        Route::put('/my_account_update', 'DashboardController@my_account_update')->name('user.my_account_update');
        Route::post('/send_question', 'DashboardController@send_question')->name('user.send_question');
});


// Login Admin
Route::get('/admin/login', 'Auth\LoginAdminController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\LoginAdminController@login');


// Grupo Admin
Route::group([
     'prefix' => 'admin',
     'namespace' => 'Admin',
     'middleware' => 'is_admin'],
      function(){
          Route::get('/', 'DashboardController@index')->name('admin.dashboard');
          Route::get('/user', 'UserController@index')->name('admin.user');
          Route::get('/question', 'QuestionController@index')->name('admin.question');
          Route::post('/edit_question', 'QuestionController@edit_question')->name('admin.edit_question');
          Route::post('/delete_question', 'QuestionController@delete_question')->name('admin.delete_question');
          Route::post('/notification_users', 'DashboardController@notification_users')->name('admin.notification_users');
          Route::post('/iframe_users', 'DashboardController@iframe_users')->name('admin.iframe_users');
          Route::post('/certificate_users', 'DashboardController@certificate_users')->name('admin.certificate_users');

          Route::get('/pdf', 'DashboardController@send_pdf')->name('admin.pdf');

          Route::get('/excel_report', 'QuestionController@excel_report')->name('admin.excel_report');

          Route::get('my_account', 'UserController@my_account')->name('admin.my_account');
          Route::put('my_account_update', 'UserController@my_account_update')->name('admin.my_account_update');
});


// Salir de sesión
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
