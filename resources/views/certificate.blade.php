<!DOCTYPE html>
<html lang="en" dir="ltr">
<style>
@page { margin: 0px; }
body{
     margin: 0px;
     background-image: url("{{ asset('/images/'.$img.'') }}");
     background-size: 100% 100%;
}
.nombre{
     margin-top: 142px;
}
.txt_nombre{
     font-size: 33px;
}
</style>
<body>
     <div class="nombre">
          <center>
               <h2 class="txt_nombre">{{ $name }}</h2>
          </center>
     </div>
</body>
</html>
