@extends('user.layout')

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="mb-3 text-white card-border bg-dark card">
             <div class="card-header">
                <i class="fas fa-video lnr-screen icon-gradient bg-warm-flame"></i>&nbsp; | Without Shadow
                <div class="btn-actions-pane-right">
                    <button class="btn btn-warning btn-sm" data-toggle="modal" data-target="#md_preg" >Realizar pregunta </button>
                </div>
            </div>
            <div class="card-body" style="padding: 0px;">
                <iframe src="https://player.vimeo.com/video/76979871" width="640" height="360" frameborder="0" autoplay="true" allowfullscreen allow="autoplay; encrypted-media"></iframe>
            </div>
        </div>
    </div>
    <div class="col-md-12">
         <dashboard-user-component>
         </dashboard-user-component>
    </div>
</div>


@stop
