<!doctype html>
<html lang="en">

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta http-equiv="Content-Language" content="en">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <meta name="_token" content="{{csrf_token()}}" />
     <title>{{ config('app.name') }}</title>
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
     />
     <meta name="description" content="This dashboard was created as an example of the flexibility that Architect offers.">

     <!-- Disable tap highlight on IE -->
     <meta name="msapplication-tap-highlight" content="no">

     <link href="{!! asset('/template/styles.css') !!}" rel="stylesheet">
     <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">
     <link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css" rel="stylesheet">

     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
     @stack('style')

     <body>
          <div class="app-container body-tabs-shadow app-theme-white">
               <div class="app-header header-shadow bg-night-sky header-text-light">
                    <div class="app-header__mobile-menu">
                         <div>
                              <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                   <span class="hamburger-box">
                                        <span class="hamburger-inner"></span>
                                   </span>
                              </button>
                         </div>
                    </div>
                    <div class="app-header__menu">
                         <span>
                              <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                   <span class="btn-icon-wrapper">
                                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                                   </span>
                              </button>
                         </span>
                    </div>
                    <div class="app-header__content">
                         <div class="app-header-right">
                              <div class="header-btn-lg pr-0">
                                   <div class="widget-content p-0">
                                        <div class="widget-content-wrapper">
                                             <div class="widget-content-left">
                                                  <div class="row">
                                                       <div class="col-sm-12">
                                                            <a href="{{ route('user.my_account')}}" class=" btn btn-sm btn-outline-warning">
                                                                 Ver Perfil
                                                            </a>

                                                            <a href="{{ url('/logout') }}" class=" btn btn-sm btn-outline-danger">
                                                                 <b>Cerrar sesion</b>
                                                            </a>
                                                       </div>
                                                  </div>


                                             </div>
                                             <div class="widget-content-left  ml-3 header-user-info">
                                                  <div class="widget-heading">
                                                       Usuario
                                                  </div>
                                                  <div class="widget-subheading">
                                                       {{ auth()->user()->name }}
                                                  </div>
                                             </div>

                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>


               <div class="app-main">


                    <div class="app-main__outer">
                         <div class="app-main__inner">
                              <div class="app-page-title" style="background: rgb(92, 110, 121); color: #fff; box-shadow: 2px 3px 5px 1px rgba(0, 0, 0, 0.1);">
                                   <div class="page-title-wrapper">
                                        <div class="page-title-heading">
                                             <div class="page-title-icon">
                                                  <i class="fab fa-vimeo-v icon-gradient bg-ripe-malin">
                                                  </i>
                                             </div>
                                             <div>Curso
                                                  <div class="page-title-subheading">Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi
                                                  </div>
                                             </div>
                                        </div>

                                   </div>
                              </div>

                              <div class="tabs-animation" id="app">
                                   @include('partials.flash-message')

                                   @yield('content')
                              </div>
                         </div>
                         <div class="app-wrapper-footer">
                              <div class="app-footer">
                                   <div class="app-footer__inner">
                                        <div class="app-footer-right">
                                             <ul class="header-megamenu nav">
                                                  <li class="nav-item">
                                                       <a data-placement="top" rel="popover-focus" data-offset="300" data-toggle="popover-custom" class="nav-link">
                                                            <p class="copyright text-center">  © 2020 Sistema Web </p>
                                                       </a>
                                                  </li>
                                             </ul>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
          <script type="text/javascript" src="{!! asset('/template/scripts/script.js') !!}"></script>
          <script src="https://player.vimeo.com/api/player.js"></script>
          <script src="{{ mix('js/app.js') }}" charset="utf-8"></script>

          <script>
          var iframe = document.querySelector('iframe');
          var player = new Vimeo.Player(iframe);

          player.on('play', function() {
               console.log('played the video!');
          });

          player.getVideoTitle().then(function(title) {
               console.log('title:', title);
          });
     </script>


     <style>
          iframe {
               width: 100%;
               height: 500px;
          }
          #swal2-content{
               text-align: justify !important;
          }
     </style>

     @stack('scripts')
</body>
</html>



@include('user.modal_question')
