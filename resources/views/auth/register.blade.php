@extends('layouts.app')

@section('content')
<h4>
    <div><h3>Bienvenido</h3></div>
    <span>Solo toma unos <span class="text-success">segundos</span> crear su cuenta</span></h4>
<div>
<form method="POST" action="{{ route('register') }}">
    @csrf
    <div class="form-row">
        <div class="col-md-6">
            <div class="position-relative form-group">
                <label for="name">{{ __('Name') }}</label>
                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="position-relative form-group">
                <label for="email">{{ __('E-Mail Address') }}</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="position-relative form-group">
                <label for="password">{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="position-relative form-group">
                <label for="password-confirm">{{ __('Confirm Password') }}</label>
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div> 
        </div>
    </div>
    <div class="divider row"></div>

    <div class="mt-4 d-flex align-items-center">
        <h5 class="mb-0">
            ¿ Ya tienes una cuenta ? 
            <a href="{{ route('login') }}" class="text-primary">Login</a>
        </h5>
        <div class="ml-auto">
            <button type="submit" class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg">
                {{ __('Register') }}
            </button>
        </div>
    </div>
</form>
                
@endsection
