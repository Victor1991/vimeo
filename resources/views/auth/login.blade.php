@extends('layouts.app')

@section('content')
<h4 class="mb-0">
    <span class="d-block"><h3>Bienvenido</h3></span>
    <span>Inicia sesión con tu cuenta.</span>
</h4>
<h6 class="mt-3">¿ Sin cuenta ? <a href="{{ route('register') }}" class="text-primary">Regístrate ahora</a></h6>
<div class="divider row"></div>
<div>
    <form method="POST" action="{{ route('login') }}">
        @csrf

        <div class="form-group row">
            <div class="col-md-6">
                <div class="position-relative form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="position-relative form-group">
                    <label for="password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

        <div class="position-relative form-check">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label" for="remember">
                {{ __('Remember Me') }}
            </label>
        </div>
        <div class="divider row"></div>

        <div class="d-flex align-items-center">
            <div class="ml-auto">
            
                @if (Route::has('password.request'))
                    <a class="btn-lg btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                @endif

                <button type="submit" class="btn btn-primary btn-lg">
                    {{ __('Login') }}
                </button>
            </div>
        <div>
    </form>
</div>
@endsection
