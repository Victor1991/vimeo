@extends('layouts.app')

@section('content')
<h4 class="mb-0">
    <span class="d-block"><h3>Bienvenido Administrador</h3></span>
    <span>Inicia sesión con tu cuenta.</span>
</h4>
<div class="divider row"></div>
<div>
    <form method="POST" action="{{ route('admin.login') }}">
        @csrf

        <div class="form-group row">
            <div class="col-md-6">
                <div class="position-relative form-group">
                    <label for="email">{{ __('E-Mail Address') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="position-relative form-group">
                    <label for="password">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>

      
        <div class="divider row"></div>

        <div class="d-flex align-items-center">
            <div class="ml-auto">

                <button type="submit" class="btn btn-primary btn-lg">
                    {{ __('Login') }}
                </button>
            </div>
        <div>
    </form>
</div>
@stop
