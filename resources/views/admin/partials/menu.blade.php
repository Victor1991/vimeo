<ul class="vertical-nav-menu">
    <li class="app-sidebar__heading">Menu</li>
    <li>
        <a href="{{ route('admin.dashboard') }}" class="{{ Request::path() ==  'admin' ? 'mm-active' : ''  }}">
            <i class="metismenu-icon fas fa-tachometer-alt"></i>   
            Dashboard
        </a>
    </li>
    <li>
        <a href="{{ route('admin.user') }}" class="{{ Request::path() ==  'admin/user' ? 'mm-active' : ''  }}">
            <i class="metismenu-icon fas fa-users"></i>
            Usuarios
        </a>
    </li>
    <li>
        <a href="{{ route('admin.question') }}" class="{{ Request::path() ==  'admin/question' ? 'mm-active' : ''  }}">
            <i class="metismenu-icon fas fa-question-circle"></i>
            Preguntas
        </a>
    </li>    
</ul>

