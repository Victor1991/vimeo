<html>
<table class="mb-0 table table-bordered table-hover table-sm table-striped">
     <thead>
          <tr>
               <th>#</th>
               <th>Pregunta</th>
               <th>Nombre</th>
               <th>Correo</th>
               <th>Contestado</th>
          </tr>
     </thead>
     <tbody>
          <?php foreach ($quetions as $key => $quetion): ?>
               <tr>
                    <th scope="row">{{ $quetion->id }}</th>
                    <td>{{ $quetion->question }}</td>
                    <td>{{ $quetion->user->name }}</td>
                    <td>{{ $quetion->user->email }}</td>
                    <td>
                         <?php if($quetion->accepted == 1): ?>
                              Si
                         <?php elseif($quetion->accepted == 2): ?>
                              No
                         <?php else: ?>
                              Sin acción
                         <?php endif; ?>
                    </td>
               </tr>
          <?php endforeach; ?>
     </tbody>
</table>
</html>
