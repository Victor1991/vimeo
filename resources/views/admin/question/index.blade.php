@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-lg-12">
          <div class="main-card mb-3 card">
               <div class="card-body">
                    <h3>Preguntas </h3>
                    <a href="{{ route('admin.excel_report') }}" target="_blank" class="mb-2 mr-2 btn btn-success" style="color:#fff; float: right; margin-top: -30px;" > <i class="fas fa-file-excel"></i> Reporte </a>
                    <br>
                    <table class="mb-0 table table-bordered table-hover table-sm table-striped">
                         <thead>
                              <tr>
                                   <th>#</th>
                                   <th>Pregunta</th>
                                   <th>Nombre</th>
                                   <th>Correo</th>
                                   <th style="text-align:center;">Opciones</th>
                              </tr>
                         </thead>
                         <tbody>
                              <?php foreach ($quetions as $key => $quetion): ?>
                                   <?php if($quetion->accepted == 1): ?>
                                        <tr class="table-success">
                                   <?php elseif($quetion->accepted == 2): ?>
                                        <tr class="table-warning">
                                   <?php else: ?>
                                        <tr>
                                   <?php endif; ?>
                                        <th scope="row">{{ $quetion->id }}</th>
                                        <td>{{ $quetion->question }}</td>
                                        <td>{{ $quetion->user->name }}</td>
                                        <td>{{ $quetion->user->email }}</td>
                                        <td style="text-align:center;">
                                             <button onclick="question_answered( {{ $quetion->id }}, '1')" type="button" class="btn btn-sm btn-success">
                                                  <i class="fa fa-check"></i>
                                             </button>
                                             <button onclick="question_answered( {{ $quetion->id }}, '2')" type="button" class="btn btn-sm btn-warning">
                                                  <i class="fas fa-times"></i>
                                             </button>
                                             <button onclick="delete_question( {{ $quetion->id }} )" type="button" class="btn btn-sm btn-danger">
                                                  <i class="fas fa-trash"></i>
                                             </button>

                                        </td>
                                   </tr>
                              <?php endforeach; ?>
                         </tbody>
                    </table>
               </div>
          </div>
     </div>
</div>
@stop
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" integrity="sha512-3QG6i4RNIYVKJ4nysdP4qo87uoO+vmEzGcEgN68abTpg2usKfuwvaYU+sk08z8k09a0vwflzwyR6agXZ+wgfLA==" crossorigin="anonymous" />
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js" integrity="sha512-v1oO0O4C4M/63o+RCahfWZjjG/NHpNiUHuGY61nlSZOTFIf1r9vlHjiWPLW4su6vIG2ZoPSiOlCRBLuFUi3uJw==" crossorigin="anonymous"></script>
<script>
function question_answered(preguna_id, type){
     axios.post('/admin/edit_question', {
          preguna_id: preguna_id,
          type: type
     })
     .then(function (response) {
          location.reload();
     })
     .catch(function (error) {
          currentObj.output = error;
     });
}

function delete_question(preguna_id) {
     var questions = this.questions;
     Swal.fire({
          title: '¿Desea eliminar la pregunta?',
          text: 'ya no podrá recuperar la pregunta',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Eliminar',
          cancelButtonText: 'Cancelar',
          showCloseButton: true,
          showLoaderOnConfirm: true
     }).then((result) => {
          axios.post('/admin/delete_question', {
               preguna_id: preguna_id
          })
          .then(function (response) {
               location.reload();
          })
          .catch(function (error) {

          });
     });
}
</script>
@endpush
