<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.brighttheme.css" rel="stylesheet">


<div class="modal fade" id="md_preg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <form action="" id="groupForm">
                    <div class="modal-header">
                         <h5 class="modal-title" id="exampleModalLongTitle">Realizar una notificación a los usuarios</h5>
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                         </button>
                    </div>
                    <div class="modal-body">
                         <p>¿ Cual es la notificación ?</p>
                         <textarea name="notifiaction" class="form-control" id="preg" required style="width:100%;" cols="30" rows="5"></textarea>
                    </div>
                    <div class="modal-footer">
                         <button type="button" id="btn-closed" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancelar</button>
                         <button type="submit" id="btn-save" class="btn btn-primary btn-lg"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar </button>
                    </div>
               </form>
          </div>
     </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js"></script>

<script>
$(document).ready(function () {

     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });
     
});

$("#groupForm").submit(function(e) {
     e.preventDefault();

     var actionType = $('#btn-save').val();
     $('#btn-save').html('  <i class="fas fa-spinner fa-pulse"></i> Sending..');

     var formData = new FormData($("#groupForm")[0]);

     $.ajax({
          // data: $('#groupForm').serialize(),
          data: formData,
          url: "{{ route('admin.notification_users') }}",
          type: "POST",
          cache: false,
          contentType: false,
          processData: false,
          dataType: 'json',
          success: function (data) {
               $("#btn-closed").trigger("click");

               $('#groupForm')[0].reset();
               $('#btn-save').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar ');
               new PNotify({
                    title: 'Mensaje enviado',
                    text: 'el mensaje de ha enviado correctamente.',
                    type: 'success',
                    delay: 8000
               });
          },
          error: function (data) {
               console.log('Error:', data);
               $('#btn-save').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar ');
               new PNotify({
                    title: 'Error al enviar mensaje',
                    text: 'intentelo más tarde',
                    type: 'error',
                    delay: 5000
               });
          }
     });
})


</script>
