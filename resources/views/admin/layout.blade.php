<!doctype html>
<html lang="en">

<head>
     <meta charset="utf-8">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta http-equiv="Content-Language" content="en">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
     <meta name="csrf-token" content="{{ csrf_token() }}">
     <title>{{ config('app.name') }}</title>
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no"
     />
     <meta name="description" content="This dashboard was created as an example of the flexibility that Architect offers.">

     <!-- Disable tap highlight on IE -->
     <meta name="msapplication-tap-highlight" content="no">

     <link href="{!! asset('/template/styles.css') !!}" rel="stylesheet"></head>
     <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet"></head>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet"></head>
     <link href="https://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.min.css" rel="stylesheet"></head>

     @stack('style')

    <body>
          <div id="app">
               <div class="app-container body-tabs-shadow app-theme-white" >
                    <div class="app-header header-shadow bg-night-sky header-text-light">
                         <div class="app-header__logo">
                              <div class="logo-src"></div>
                              <div class="header__pane ml-auto">
                                   <div>
                                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                             <span class="hamburger-box">
                                                  <span class="hamburger-inner"></span>
                                             </span>
                                        </button>
                                   </div>
                              </div>
                         </div>
                         <div class="app-header__mobile-menu">
                              <div>
                                   <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                        <span class="hamburger-box">
                                             <span class="hamburger-inner"></span>
                                        </span>
                                   </button>
                              </div>
                         </div>
                         <div class="app-header__menu">
                              <span>
                                   <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                        <span class="btn-icon-wrapper">
                                             <i class="fa fa-ellipsis-v fa-w-6"></i>
                                        </span>
                                   </button>
                              </span>
                         </div>
                         <div class="app-header__content">
                              <div class="app-header-right">

                                   <div class="header-dots">
                                        <button type="button" class="p-0 mr-2 btn btn-link" data-toggle="modal" data-target="#md_iframe">
                                             <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                                                  <span class="icon-wrapper-bg bg-danger"></span>
                                                  <i class="icon text-danger icon-anim-pulse ion-android-globe"></i>
                                             </span>
                                        </button>
                                        <button type="button" class="p-0 mr-2 btn btn-link" data-toggle="modal" data-target="#md_preg">
                                             <span class="icon-wrapper icon-wrapper-alt rounded-circle">
                                                  <span class="icon-wrapper-bg bg-danger"></span>
                                                  <i class="icon text-danger icon-anim-pulse ion-android-notifications"></i>
                                             </span>
                                        </button>
                                   </div>

                                   <div class="header-btn-lg pr-0">
                                        <div class="widget-content p-0">
                                             <div class="widget-content-wrapper">
                                                  <div class="widget-content-left">

                                                       <div class="btn-group">
                                                            <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                                                 <img width="42" class="rounded-circle" src="https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png" alt="">
                                                                 <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                                            </a>
                                                            <div tabindex="-1" role="menu" aria-hidden="true" class="rm-pointers dropdown-menu-lg dropdown-menu dropdown-menu-right">
                                                                 <div class="dropdown-menu-header">
                                                                      <div class="dropdown-menu-header-inner bg-info">
                                                                           <div class="menu-header-image opacity-2" style="background-image: url('assets/images/dropdown-header/city3.jpg');"></div>
                                                                           <div class="menu-header-content text-left">
                                                                                <div class="widget-content p-0">
                                                                                     <div class="widget-content-wrapper">
                                                                                          <div class="widget-content-left mr-3">
                                                                                               <img width="42" class="rounded-circle"
                                                                                               src="https://img.pngio.com/parent-directory-avatar-2png-avatar-png-256_256.png"
                                                                                               alt="">
                                                                                          </div>
                                                                                          <div class="widget-content-left">
                                                                                               <div class="widget-heading">{{ auth()->user()->name }}
                                                                                               </div>
                                                                                               <div class="widget-subheading opacity-8">{{ auth()->user()->email }}
                                                                                               </div>
                                                                                          </div>
                                                                                     </div>
                                                                                </div>
                                                                           </div>
                                                                      </div>
                                                                 </div>

                                                                 <div class="grid-menu grid-menu-2col">
                                                                      <div class="no-gutters row">
                                                                           <div class="col-sm-6">
                                                                                 <a href="{{ route('admin.my_account')}}" class="btn-icon-vertical btn-transition btn-transition-alt pt-2 pb-2 btn btn-outline-warning">
                                                                                     <i class="fa fa-user icon-gradient bg-amy-crisp btn-icon-wrapper mb-2" aria-hidden="true"></i>
                                                                                     Ver Perfil
                                                                                </a>
                                                                           </div>
                                                                           <div class="col-sm-6">
                                                                                <a href="{{ url('/logout') }}" class="btn-icon-vertical btn-transition btn-transition-alt pt-2 pb-2 btn btn-outline-danger">
                                                                                     <i class="fas fa-sign-out-alt icon-gradient bg-love-kiss btn-icon-wrapper mb-2"></i>
                                                                                     <b>Cerrar sesion</b>
                                                                                </a>
                                                                           </div>
                                                                      </div>
                                                                 </div>

                                                            </div>
                                                       </div>
                                                  </div>
                                                  <div class="widget-content-left  ml-3 header-user-info">
                                                       <div class="widget-heading">
                                                            Alina Mclourd
                                                       </div>
                                                       <div class="widget-subheading">
                                                            VP People Manager
                                                       </div>
                                                  </div>

                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>


                    <div class="app-main">
                         <div class="app-sidebar sidebar-shadow bg-royal sidebar-text-light">
                              <div class="app-header__logo">
                                   <div class="logo-src"></div>
                                   <div class="header__pane ml-auto">
                                        <div>
                                             <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                                  <span class="hamburger-box">
                                                       <span class="hamburger-inner"></span>
                                                  </span>
                                             </button>
                                        </div>
                                   </div>
                              </div>
                              <div class="app-header__mobile-menu">
                                   <div>
                                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                                             <span class="hamburger-box">
                                                  <span class="hamburger-inner"></span>
                                             </span>
                                        </button>
                                   </div>
                              </div>
                              <div class="app-header__menu">
                                   <span>
                                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                                             <span class="btn-icon-wrapper">
                                                  <i class="fa fa-ellipsis-v fa-w-6"></i>
                                             </span>
                                        </button>
                                   </span>
                              </div>
                              <div class="scrollbar-sidebar">
                                   <div class="app-sidebar__inner">
                                        @include('admin.partials.menu')
                                   </div>
                              </div>
                         </div>

                         <div class="app-main__outer">
                              <div class="app-main__inner">
                                   <div class="app-page-title" style=" box-shadow: 2px 3px 5px 1px rgba(0, 0, 0, 0.1);">
                                        <div class="page-title-wrapper">
                                             <div class="page-title-heading">
                                                  <div class="page-title-icon">
                                                       <i class="fas fa-tachometer-alt icon-gradient bg-ripe-malin">
                                                       </i>
                                                  </div>
                                                  <div>Dashboard
                                                       <div class="page-title-subheading">Representación gráfica de las principales métricas
                                                       </div>
                                                  </div>

                                             </div>
                                        </div>

                                   </div>

                                   <div class="tabs-animation">
                                        @include('partials.flash-message')

                                        @yield('content')

                                   </div>
                              </div>
                              <div class="app-wrapper-footer">
                                   <div class="app-footer">
                                        <div class="app-footer__inner">
                                             <div class="app-footer-right">
                                                  <ul class="header-megamenu nav">
                                                       <li class="nav-item">
                                                            <a data-placement="top" rel="popover-focus" data-offset="300" data-toggle="popover-custom" class="nav-link">
                                                                 <p class="copyright text-center">  © 2020 Sistema Web </p>
                                                            </a>
                                                       </li>
                                                  </ul>
                                             </div>
                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
          <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
          <script type="text/javascript" src="{!! asset('/template/scripts/script.js') !!}"></script>
          <script src="{{ mix('js/app.js') }}" charset="utf-8"></script>

          @stack('scripts')

          <style>
               .vertical-nav-menu li a{
                    line-height: 3.4rem !important;
                    height: 3.4rem  !important;
               }
          </style>

          <style media="screen">
               .alert button.close{
                    right: 25px;
                    top: 40%;
               }
          </style>
          @include('admin.modal_notificacion')
          @include('admin.modal_iframe')
          @include('admin.modal_img_certificado')
     </body>
</html>
