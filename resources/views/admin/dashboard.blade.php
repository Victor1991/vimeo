@extends('admin.layout')

@section('content')

<dashboard-component
     :prop_users="<?= htmlspecialchars(json_encode($users), ENT_QUOTES, 'UTF-8'); ?>"
     :prop_questions="<?= htmlspecialchars(json_encode($questions), ENT_QUOTES, 'UTF-8'); ?>"
>
</dashboard-component>


@stop