@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-lg-12">
          <div class="main-card mb-3 card">
               <div class="card-body">
                    <h3>Usuarios </h3>
                    <button id="checked" class="mb-2 mr-2 btn btn-success" style="color:#fff; float: right; margin-top: -30px;" ><i class="far fa-check-square"></i> Seleccionar todo </button>
                    <br>
                    <form id="table_users" method="post">
                         <table class="mb-0 table table-bordered table-hover table-sm table-striped">
                              <thead>
                                   <tr>
                                        <th>#</th>
                                        <th>Nombre</th>
                                        <th>Correo</th>
                                        <th>Preguntas</th>
                                        <th style="text-align:center;">Certificado</th>
                                   </tr>
                              </thead>
                              <tbody>
                                   <?php foreach ($users as $key => $user): ?>
                                        <tr>
                                             <th scope="row">{{ $user->id }}</th>
                                             <td>{{ $user->name }}</td>
                                             <td>{{ $user->email }}</td>
                                             <td>{{ $user->questions->count() }}</td>
                                             <td style="text-align:center;">
                                                  <input class="item" type="checkbox" name="certificado[]" value="<?=$user->id?>">
                                             </td>
                                        </tr>
                                   <?php endforeach; ?>

                              </tbody>
                         </table>
                    </form>
                    <br>
                    <button type="button" id="enviar_cenrtificado"  data-toggle="modal" data-target="#md_imagen" class="mb-2 mr-2 btn btn-success" style="float: right;" name="button"> <i class="fas fa-paper-plane"></i> Enviar certificado</button>
               </div>
          </div>
     </div>
</div>

@stop
@push('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.css" integrity="sha512-3QG6i4RNIYVKJ4nysdP4qo87uoO+vmEzGcEgN68abTpg2usKfuwvaYU+sk08z8k09a0vwflzwyR6agXZ+wgfLA==" crossorigin="anonymous" />
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.js" integrity="sha512-v1oO0O4C4M/63o+RCahfWZjjG/NHpNiUHuGY61nlSZOTFIf1r9vlHjiWPLW4su6vIG2ZoPSiOlCRBLuFUi3uJw==" crossorigin="anonymous"></script>
<script>
$( '#checked' ).click( function () {
     $(':checkbox.item').prop('checked', true);
});
$( '#enviar_cenrtificado' ).click( function (event) {
     if($("input[type='checkbox']").is(':checked') === false){
          Swal.fire(
               'Es requerido el usuario',
               'Es requerido al menos un usuario',
               'warning'
          )
          event.stopPropagation();
     }
});
</script>
@endpush
