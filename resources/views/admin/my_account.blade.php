@extends('admin.layout')

@section('content')
<div class="row">
     <div class="col-md-8 mx-auto">
          <div class="main-card mb-3 card">
               <div class="card-body"><h5 class="card-title"> Editar perfil</h5>
               <hr>
               <form class="form" method="post" action="{{ route('admin.my_account_update', $user) }}">
                         {{ csrf_field() }} {{ method_field('PUT') }}
                         <div class="row">
                              <div class="col-md-12">
                                   @if($errors->any())
                                   <ul class="list-group">
                                        @foreach($errors->all() as $key => $error)
                                        <li class="list-group-item list-group-item-danger"> {{ $error }}</li>
                                        @endforeach
                                   </ul>
                                   @endif
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Nombre</label>
                                        <input type="text" placeholder="Nombre" name="name" class="form-control"  value="{{ $user->name }}">
                                   </div>
                              </div>
                              
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Correo</label>
                                        <input type="email"  value="{{  $user->email }}" name="email" placeholder="Correo" class="form-control">
                                   </div>
                              </div>
                              
                              
                              
                         </div>
                         <div class="row">
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Contraseña</label>
                                        <input type="password" name="password" placeholder="Contraseña" class="form-control">
                                   </div>
                              </div>
                              <div class="col-md-6">
                                   <div class="form-group">
                                        <label>Confirmar Contraseña</label>
                                        <input type="password" name="password_confirmation" placeholder="Confirmar contraseña" class="form-control">
                                   </div>
                              </div>
                         </div>
                         <hr>
                         
                         <a  href="{{ route('admin.dashboard') }}" class="btn btn-fill btn-wd btn-default">
                              <i class="fa fa-arrow-left" aria-hidden="true"></i>
                              Regresar
                         </a>
                         <button type="submit" class="btn btn-fill btn-success btn-wd pull-right">
                              <i class="fa fa-floppy-o" aria-hidden="true"></i>
                              Actualizar información
                         </button>
                    </form>
               </div>
          </div>
     </div>
</div>
@stop
@push('scripts')

@endpush

@push('style')
@endpush
