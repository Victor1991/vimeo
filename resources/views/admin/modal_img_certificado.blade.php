<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.buttons.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.brighttheme.css" rel="stylesheet">


<div class="modal fade" id="md_imagen" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Selecione la imagen del certificado</h5>
               </div>
               <div class="modal-body">
                    <div class="input-group mb-3">
                         <div class="custom-file">
                              <input type="file" class="custom-file-input" accept="image/*" id="img_certificado">
                              <label class="custom-file-label" for="inputGroupFile01">Seleccinar archvio</label>
                         </div>
                    </div>
                    <div class="col-md-12">
                         <div id="exito">

                         </div>
                         <div id="error">

                         </div>


                    </div>
               </div>
               <div class="modal-footer">
                    <button type="button" id="btn-closed-certificate" class="btn btn-secondary btn-lg" data-dismiss="modal">Cerrar</button>
                    <button type="button" id="btn-send-certificate" class="btn btn-primary btn-lg"><i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar </button>
               </div>
          </div>
     </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pnotify/3.2.1/pnotify.js"></script>

<script>
$(document).ready(function () {

     $.ajaxSetup({
          headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
     });

});

$(".custom-file-input").on("change", function() {
     var fileName = $(this).val().split("\\").pop();
     $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$("#btn-send-certificate").click( function (event) {
     if($("#img_certificado").val() == ""){
          Swal.fire(
               'Es requerida la imagen',
               'Es requerida la imagen del certificado',
               'warning'
          )
          return false;
     }

     var actionType = $('#enviar_informacion').val();
     $('#btn-send-certificate').html(' <i class="fas fa-spinner fa-pulse"></i> Sending..');
     $('#btn-send-certificate').prop( "disabled", true );
     $('#btn-closed-certificate').hide();

     var formData = new FormData($("#table_users")[0]);
     var files = $('#img_certificado')[0].files[0];
     formData.append('img_certificado',files);

     $.ajax({
          data: formData,
          url: "{{ route('admin.certificate_users') }}",
          type: "POST",
          cache: false,
          enctype: 'multipart/form-data',
          contentType: false,
          processData: false,
          dataType: 'json',
          success: function (data) {
               $('#img_certificado').val('');
               $('#btn-closed-certificate').show();
               $("#btn-closed-iframe").trigger("click");
               $('#btn-send-certificate').prop( "disabled", false );
               $('#table_users')[0].reset();
               $('#btn-send-certificate').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar ');
               new PNotify({
                    title: 'Mensaje enviado',
                    text: 'el mensaje de ha enviado correctamente.',
                    type: 'success',
                    delay: 8000
               });
               var exito = 0;
               var mens_exit = '';
               var error = 0;
               var mens_error = '';
               if (data.type == 'exito') {
                    data.mensaje.forEach(function(elemento) {
                         if (elemento.type == "exito") {
                              mens_exit += elemento.mensaje+'<br>';
                              exito++;
                         }else {
                              mens_error += elemento.mensaje+'<br>';
                              error++;
                         }
                    });
               }
               if (exito > 0) {
                    $( "#exito" ).append( '<div class="alert alert-success alert-dismissible fade show" role="alert">' +
                    '<strong>Enviados correctamente : </strong>'+mens_exit+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>' );
               }
               if (error > 0) {
                    $( "#error" ).append( '<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                    '<strong>Enviados correctamente : </strong>'+mens_error+
                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '<span aria-hidden="true">&times;</span>' +
                    '</button>' +
                    '</div>' );
               }
               $('.custom-file-input').siblings(".custom-file-label").addClass("selected").html('');

          },
          error: function (data) {
               $('#img_certificado').val('');
               $('#btn-closed-certificate').show();
               $("#btn-closed-iframe").trigger("click");
               $('#btn-send-certificate').prop( "disabled", false );
               $('#btn-send-certificate').html('<i class="fa fa-paper-plane" aria-hidden="true"></i> Enviar ');
               $('#table_users')[0].reset();
               new PNotify({
                    title: 'Error al enviar mensaje',
                    text: 'intentelo más tarde',
                    type: 'error',
                    delay: 5000
               });
          }
     });
})


</script>
