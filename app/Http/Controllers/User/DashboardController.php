<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Question;
use Response;
use App\Events\NewMessage;



class DashboardController extends Controller
{
    public function index()
    {
         return view('user.dashboard');
    }
    public function my_account()
     {
          $user = Auth::user();
          return view('user.my_account', compact('user'));
     }

     public function my_account_update(Request $request)
     {
          $user = User::where('id', Auth::user()->id)->first();

          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
               $data['password'] = bcrypt(request('password'));
          }

          $data['name'] = request('name');
          
          $user->update( $data );

          return redirect()->route('user.dashboard')->with('success', 'Informacón actualizada');
     }

     public function send_question(Request $request)
     {
          // dump($request);
          $data['question'] = request('question');
          $data['user_id'] = Auth::user()->id;
          $question = Question::create($data);

          $fecha = date("Y-m-d H:i:s", strtotime($question['created_at']));
          NewMessage::dispatch( 
               $question['id'], 
               $question['question'], 
               $question['user_id'], 
               $fecha
          );

          return Response::json($question);

     }
}
