<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

use User;




class LoginAdminController extends Controller
{
     /*
     |--------------------------------------------------------------------------
     | Login Controller
     |--------------------------------------------------------------------------
     |
     | This controller handles authenticating users for the application and
     | redirecting them to your home screen. The controller uses a trait
     | to conveniently provide its functionality to your applications.
     |
     */

     use AuthenticatesUsers;

     /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/admin';

     /**
     * Create a new controller instance.
     *
     * @return void
     */
     public function __construct()
     {
          $this->middleware('guest')->except('logout');
     }

     public function showLoginForm()
     {
          return view('auth.login_admin');
     }


     public function login(Request $request)
     {
          $this->validate($request, [
               'email' => 'required',
               'password' => 'required',
          ]);

          $admin = \App\User::where('email', $request->email)->first();

          if (!$admin) {
               return redirect('admin/login')->with('error', 'No existe el usuario con este correo');
          }

          if (Hash::check($request->password, $admin->password)) {
               Auth::guard('web')->login($admin);
               return redirect('/admin');
          }

          return redirect('admin/login')
          ->withInput($request->only('email', 'remember'))
          ->withErrors(['email' => 'Dirección de correo electrónico o contraseña incorrecta']);
     }
}
