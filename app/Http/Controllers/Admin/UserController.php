<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
     public function index()
     {
          // $users = User::where('is_admin', 0)->get();
          $users = User::all();
          return view('admin.user.index', compact('users'));
     }

     public function my_account()
     {
          $user = User::where('id', Auth::user()->id)->first();
          return view('admin.my_account', compact('user'));
     }

     public function my_account_update(Request $request)
     {
          $user = User::where('id', Auth::user()->id)->first();
          $data = $request->validate([
               'name' => 'required|string|max:255',
               'email' => 'required|string|email|max:255|unique:users,email,'.$user->id,
          ]);

          if($request->filled('password')) {
               $data =  $request->validate([
                    'password' => 'confirmed', 'min:7',
               ]);
               $data['password'] = bcrypt(request('password')) ;

          }

          $data['surnames'] = request('surnames') ;
          $user->update( $data );

          return redirect()->route('admin.dashboard')->with('success', 'Información actualizada');
     }
}
