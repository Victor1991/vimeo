<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Exports\QuestionExport;

use App\Question;

class QuestionController extends Controller
{
    public function index()
    {
         $quetions = Question::all();
         return view('admin.question.index', compact('quetions'));
    }

    public function edit_question(Request $request)
    {
        $question = Question::where('id', request('preguna_id'))->first();
        $data['accepted'] = request('type');
        $question->update( $data );
        return response()->json( $question );
    }

    public function delete_question(Request $request)
    {
         $res = Question::where('id',request('preguna_id'))->delete();
         return response()->json( $res );
    }

    public function excel_report()
    {
         return(new QuestionExport)->download('preguntas.xlsx');
    }
}
