<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Question;
use App\NotificationUser;
use App\IframeUser;
use Response;
use App\Events\NotificationUsers;
use App\Events\IframeUsers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mail;
use PDF;

class DashboardController extends Controller{

     public function index(){
          $users = User::where('is_admin', '0')->get();
          $questions = Question::orderBy('created_at', 'desc')->get();
          return view('admin.dashboard', compact('users', 'questions'));
     }

     public function notification_users(Request $request){
          $noti = new NotificationUser;
          $noti->notification = request('notifiaction');
          $noti->save();

          NotificationUsers::dispatch(
               request('notifiaction')
          );

          return Response::json($noti);
     }

     public function iframe_users(Request $request){
          $noti = new IframeUser;
          $noti->iframe = request('iframe');
          $noti->save();

          IframeUsers::dispatch(
               request('iframe')
          );

          return Response::json($noti);
     }

     public function certificate_users(Request $request){
          $image = $request->file('img_certificado');
          $new_name = rand() . '.' . $image->getClientOriginalExtension();
          $image->move(public_path('images'), $new_name);
          $arreglo = array();
          foreach (request('certificado') as $key => $usuario_id) {
               $arreglo[] = $this->send_pdf($usuario_id, $new_name);
          }
          $mensaje = array('mensaje' => $arreglo, 'type' => 'exito');
          return Response::json($mensaje);
     }

     public function send_pdf($id, $img){
          // $id = 1;
          // $img = '848272493.jpg';
          $user = User::where('id', $id)->first()->toarray();
          $user['img'] = $img;

          $pdf = \PDF::loadView('certificate', $user);
          $pdf->setPaper('a4', 'landscape');
          // return $pdf->stream();          // die;

          Mail::send('certificate', $user, function($message)use($user, $pdf){
               $message->to($user["email"], $user["name"])->subject('Certificado de sistema vimeo a:'. $user["name"]);
               $message->attachData($pdf->output(), 'certificado.pdf');
          });

          if (Mail::failures()) {
               return array('mensaje' => 'Error al enviado a '.$user["email"], 'type' => 'error');
          }else{
               return array('mensaje' => 'Enviado a '.$user["email"], 'type' => 'exito');
          }
     }
}
