<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
     protected $fillable = [
          'question', 'user_id', 'accepted',
     ];

     public function user() {
          return $this->hasOne(User::class, 'id', 'user_id');
     }
}
