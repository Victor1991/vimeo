<?php

namespace App\Exports;

use App\Question;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\Exportable;


class QuestionExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    use Exportable;

    private $date;


    public function view(): View
    {
         return view('admin.question.table', [
              'quetions' => Question::all()
         ]);
    }
}
